var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')

//va a lanzar la aplicación en segundo plano para probar que funciona
var server =require('../server')

var should = chai.should()


//configurar chai con modulo http
chai.use(chaiHttp)
var expect = chai.expect;


describe ('Test de Api usuarios',()=>
{
  it('nuevo usuario',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .post('/apitechu/v5/usuarios/nuevo')
      .set('dni', '03473256a')
      .set('nombre', 'David')
      .set('apellidos', 'de antonio')
      .set('email', 'david@bbva.com')
      .set('password', '111111')

      .end((err, res) =>
      {
        //es correcto porque no le paso ningun iban al usuario
        res.should.have.status(201)

        done() //para cerrar la operacion
      })
  })
  it('cuentas iban cliente correcto sin cuentas',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .get('/apitechu/v5/clienteIban')
      .set('idcliente', '03473256a')

      .end((err, res) =>
      {

        res.should.have.status(400)

        done() //para cerrar la operacion
      })
  })
  it('nueva cuenta',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .post('/apitechu/v5/clienteIban/nuevaCuenta')
      .set('dni','03473256a')
      .set('iban','ES1720852066623456789011')
      .set('nombrecuenta', 'vivienda')


      .end((err, res) =>
      {
        //es correcto porque no le paso ningun iban al usuario
        res.should.have.status(200)

        done() //para cerrar la operacion
      })
  })

  it('cuentas iban cliente correcto con cuentas',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .get('/apitechu/v5/clienteIban')
      .set('idcliente', '03473256a')

      .end((err, res) =>
      {

        res.should.have.status(200)

        done() //para cerrar la operacion
      })
  })
  it('cliente incorrecto',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .get('/apitechu/v5/clienteIban')
      .set('idcliente', '11')

      .end((err, res) =>
      {
        //es correcto porque no le paso ningun iban al usuario
        res.should.have.status(400)

        done() //para cerrar la operacion
      })
  })
  it('logout usuario',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .post('/apitechu/v5/usuarios/logout')
      .set('dni','03473256a')

      .end((err, res) =>
      {
        //es correcto porque no le paso ningun iban al usuario
        res.should.have.status(200)

        done() //para cerrar la operacion
      })
  })
  it('login usuario',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .post('/apitechu/v5/usuarios/login')
      .set('dni','03473256a')
        .set('password', '111111')

      .end((err, res) =>
      {
        //es correcto porque no le paso ningun iban al usuario
        res.should.have.status(200)

        done() //para cerrar la operacion
      })
  })
  it('cuentas iban de usuario creado',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .get('/apitechu/v5/clienteIban')
      .set('idcliente', '03473256a')

      .end((err, res) =>
      {

        res.should.have.status(200)

        done() //para cerrar la operacion
      })
  })
  it('nuevo movimiento',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .post('/apitechu/v5/movimientos/nuevo')
      .set('idcliente', '03473256a')

      .set('iban', 'ES1720852066623456789011')
      .set('descripcion', 'Nuevo movimiento prueba')
      .set('importe', '30')
      .set('concepto', 'Pago')
      .set('detalle', 'colegio')
      .set('longitud', '-3.666226')
      .set('latitud', '40.501577')
      .set('ubicacion', 'www.google.com')


      .end((err, res) =>
      {

        res.should.have.status(201)

        done() //para cerrar la operacion
      })
  })
  it('visor movimiento',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .get('/apitechu/v5/movimientos')
      .set('iban', 'ES1720852066623456789011')
      .end((err, res) =>
      {

        res.should.have.status(200)
        done() //para cerrar la operacion
      })
  })
  it('borrar cuenta ',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .delete('/apitechu/v5/eliminarCuenta')
      .set('iban','ES1720852066623456789011')
      .set('dni', '03473256a')
      .end((err, res) =>
      {

        res.should.have.status(200)
        done() //para cerrar la operacion
      })
  })
  it('eliminar usuario ',(done) =>
  {

    chai.request('http://apifinal-apitechujdea.7e14.starter-us-west-2.openshiftapps.com')
      .delete('/apitechu/v5/usuarios/borrar')
      .set('idcliente', '03473256a')
      .set('dni', '03473256a')
      .end((err, res) =>
      {

        res.should.have.status(200)
        done() //para cerrar la operacion
      })
  })




})
