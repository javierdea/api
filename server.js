//crea un servidor ligero
var express=require('express');

//para hasear la contraseña
var sha256 = require('sha256');
//para el token de apikey
var jwt = require('jwt-simple');

//inicializa
var app=express()
//referencia a la libreria de body-parser
var bodyParser = require('body-parser')
app.use(bodyParser.json())


//para activar el cors de las peticiones
var cors = require('cors');
app.use(cors())
//para leer los bodys tiene que usar JSON


var config = require('./config');
var apiKey= config.TOKEN_SECRET

//crear la varible de request-json para poder referenciarla
var requestJson=require ('request-json')

//vamos a crear un cliente del mlab, para las peticiones http
var urlMlabRaiz="https://api.mlab.com/api/1/databases/bdbancojdea/collections/"

//mi apikey para conectarme a mlab
//var apiKey="apiKey=Wb9-f2YfLR4yqfzqKpgZPvzFFGoUdk8A"
// la estoy metiendo en un fichero config por temas de seguridad por si me la detectan. y ademas en el gitignore digo que no me lo suba a git

//creo el cliente y lo inicializo a null porque no se si más adelante lo voy a invocar
var clienteMlab= null


//------------------------------------------------------------------------------------------------------
//--------NOMBRE:USUARIOS/LOGIN-------------------------------------------------------------------------------
//--------descripcion:FUNCION QUE LOGUEA A UN USUARIO PONIENDOLE EL LOGIN=TRUE-------------------------------
//--------PARAM ENTRADA:DNI Y CONTRASEÑA DEL USUARIO A HACER LOGIN---------------------------------------
//--------PARAM SALIDA:USUARIO LOGADO O NO SI HA HABIDO ALGUN PROBLEMA-------------------------------------------
//------------------------------------------------------------------------------------------------------
//funcion que recorre el fichero para buscar si el usuario ya se encuentra en el fichero para poner el login=true

app.post('/apitechu/v5/usuarios/login',function (req,res)
{

    var dni = req.headers.dni
    var password =sha256(req.headers.password);
    console.log("este es el passwors haseado",password)
    //defino la query donde le voy a pasar el dni y password para ver si ya existe
    var query = 'q={"dni":"' + dni + '","password":"' + password + '"}'
    //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
    clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA?"+ query +"&l=1&" + apiKey)
    //console.log("entro",clienteMlab) //comprueba que se esté formando bien la url
    //resM = la respuesta de mlab
    //como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
    clienteMlab.get('',function(err,resM,body){
      if (!err){
        //si no hay ninguno no me debe devolver nada, sino que devuelva el primero
        if (body.length == 1)
        {
          //vamos a meter en dicho objeto encontrado que el usuario se ha logado. Utilizamos el put
          //el segundo valor del put,del delete y del update es el body, por eso le paso un JSON JSON.parse(cambio)

            clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA")
            var cambio='{"$set":{"login":true}}'
            console.log(clienteMlab) //comprueba que se esté formando bien la url

            clienteMlab.put('?q={"dni":"'+dni + '"}&'+apiKey,JSON.parse(cambio),function(errP,resP,bodyP)
            {

              res.status(200).send({"dni":body[0].dni,"nombre":body[0].nombre,"email":body[0].email,"apellidos":body[0].apellidos})
              console.log("El usuario", body[0].dni," y email",body[0].email, "se ha logado correctamente")
            })
          }
          else
          {
            console.log("no hay ningun usuario con el dni=",dni)
            res.status(404).send('Usuario no encontrado')
          }
        }
    })
})
//------------------------------------------------------------------------------------------------------
//--------NOMBRE:USUARIOS/LOGOUT-------------------------------------------------------------------------------
//--------descripcion:FUNCION QUE HACE EL LOGOUT DE UN USUARIO PONIENDOLE EL LOGIN=FALSE-------------------------------
//--------PARAM ENTRADA:DNI DEL USUARIO A HACER LOGOUT---------------------------------------
//--------PARAM SALIDA:USUARIO HECHO EL LOGOUT O NO SI HA HABIDO ALGUN PROBLEMA-------------------------------------------
//------------------------------------------------------------------------------------------------------
app.post ('/apitechu/v5/usuarios/logout',function (req,res)
{
    var dni = req.headers.dni
    //buscamos que el usuarios con dicho , tenga la variable de login:true
    var query = 'q={"dni":"' + dni + '","login":true}'
    //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
    clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA?"+ query +"&l=1&" + apiKey)
    console.log(clienteMlab) //comprueba que se esté formando bien la url
    //resM = la respuesta de mlab
    //como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
    clienteMlab.get('',function(err,resM,body)
    {
      if (!err)
      {
        //si no hay ninguno no me debe devolver nada, sino que devuelva el primero
        if (body.length == 1)
        {
          //vamos a meter en dicho objeto encontrado que el usuario se ha logado. Utilizamos el put
          //el segundo valor del put,del delete y del update es el body, por eso le paso un JSON JSON.parse(cambio)
            clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA")
            var cambio='{"$set":{"login":false}}'
            console.log(clienteMlab) //comprueba que se esté formando bien la url

            clienteMlab.put('?q={"dni":"'+dni + '"}&'+apiKey,JSON.parse(cambio),function(errP,resP,bodyP)
            {

              res.status(200).send('El usuario ha salido correctamente de la aplicación')
              console.log("El usuario", body[0].dni," ha hecho el logout correctamente")
            })
          }
          else
          {
            console.log("Usuario no logado correctamente")
            res.status(400).send('Usuario no logado correctamente')
          }
        }
    })
})
//------------------------------------------------------------------------------------------------------
//--------NOMBRE:USUARIOS/NUEVO-------------------------------------------------------------------------------
//--------descripcion:FUNCION QUE CREA UN NUEVO USUARIO -------------------------------
//--------PARAM ENTRADA:DNI, NOMBRE, APELLIDOS, EMAIL Y CONTRASEÑA DEL USUARIO NUEVO---------------------------
//--------PARAM SALIDA:USUARIO NUEVO CREADO O NO SI YA EXISTE EN EL FICHERO------------------------------------
//------------------------------------------------------------------------------------------------------

app.post ('/apitechu/v5/usuarios/nuevo',function (req,res)
{
    var dni=req.headers.dni
    var nombre = req.headers.nombre
    var apellidos = req.headers.apellidos
    var email = req.headers.email
    var password =sha256(req.headers.password);
    console.log("este es el passwors haseado",password)
    var nuevo={"dni":dni,"nombre":nombre,"apellidos":apellidos,"email":email,"password":password,"login": true}

    var query = 'q={"dni":"' + dni + '"}'
    //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
    clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA?"+ query +"&l=1&" + apiKey)
    //console.log(clienteMlab) //comprueba que se esté formando bien la url
    clienteMlab.get('',function(err,resM,body)
    {
      if (!err)
      {
        //si no hay ninguno no se deberia insertar en la base de datos porque ya existe un
        if (body.length == 0)
        {
                //creo la varibale cliente que es donde voy a montar mi url a la que haré un post
                clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA?" + apiKey)
                console.log(clienteMlab) //comprueba que se esté formando bien la url

                //como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
                clienteMlab.post('',nuevo,function(err,resM,body)
                {
                  if (!err){
                    //sino da error es que se ha insertado

                    res.status(201).send({"dni":dni,"apellidos":apellidos})
                    console.log("Usuario dado de alta")
                  }
                  else
                  {
                    console.log("usuario no dado de alta")
                    res.status(400).send('Usuario no logado correctamente')
                  }
                })
          }
          //si es mayor que 0 es que ya existe alguno
          else
          {
            console.log("ya existe un usuario con ese DNI")
            res.status(400).send('Usuario no dado de alta por ya existir dicho DNI')
          }
        }
      })
})
//------------------------------------------------------------------------------------------------------
//--------NOMBRE:USUARIOS/BOORRAR-------------------------------------------------------------------------------
//--------descripcion:FUNCION QUE CREA UN NUEVO USUARIO -------------------------------
//--------PARAM ENTRADA:DNI, NOMBRE, APELLIDOS, EMAIL Y CONTRASEÑA DEL USUARIO NUEVO---------------------------
//--------PARAM SALIDA:USUARIO NUEVO CREADO O NO SI YA EXISTE EN EL FICHERO------------------------------------
//------------------------------------------------------------------------------------------------------

app.delete('/apitechu/v5/usuarios/borrar',function (req,res)
{
    var dni=req.headers.dni

    var query = 'q={"dni":"' + dni + '"}'
    var filter='f={"_id":1}&'


    //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
    clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA?"+ query +filter + apiKey)
    clienteMlab.get('',function(err,resM,body){

      if (!err)
      {

        //si no hay ninguno no me debe devolver nada, sino que devuelva el primero
        if (body.length == 1)
        {
          //vamos a meter en dicho objeto encontrado que el usuario se ha logado. Utilizamos el put
          //el segundo valor del put,del delete y del update es el body, por eso le paso un JSON JSON.parse(cambio)

          clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA/"+ body[0]._id.$oid + "?"+ apiKey)

          clienteMlab.del('',function(err,resM,body)
            {
              console.log('vamos a eliminar al cliente con id=', dni)
              if (!err)
              {
                  console.log(body)
                  console.log(resM.statusCode)
                  res.status(200).send(body)
                  console.log("Se ha eliminado correctamente al usuario con dni",dni)

                }
              else
              {
                console.log("No se ha eliminado al usuario con dni",dni)
                res.status(400).send("No se ha eliminado al usuario con dni"+dni)
              }
          })
        }
      }
      else
      {
        console.log("no está en la BBDD")
        res.status(400).send('no está en la BBDD')}
    })
    })


//--
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------


app.post ('/apitechu/v5/clienteIban/nuevaCuenta',function (req,res){

var dni = req.headers.dni
var iban = req.headers.iban
var nombrecuenta= req.headers.nombrecuenta
var movimientos=[]
var saldo=0
  console.log('vamos a crear una cuenta al cliente con id=', dni)

  var nuevo={"iban":iban,"nombrecuenta":nombrecuenta,"idcliente":dni,"saldo":saldo,"movimientos":movimientos}

  var query = 'q={"iban":"' + iban + '","idcliente":"'+dni+'"}"'

  //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
  clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ query +"&l=1&" + apiKey)
  console.log(clienteMlab) //comprueba que se esté formando bien la url
  clienteMlab.get('',function(err,resM,body)
  {

        if (!err)
        {

          if (body.length ==0)
          {
            //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
            clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ apiKey)
            clienteMlab.post('',nuevo,function(err,resM,body)
              {
                if (!err)
                {
                    res.status(200).send("Se ha creado correctamente la cuenta del usuario con dni"+dni)
                    console.log("Se ha creado correctamente la cuenta del usuario con dni",dni)

                  }
                  else
                  {
                    console.log("No se ha creado la cuenta del usuario con dni",dni)
                    res.status(400).send("No se ha creado la cuenta del usuario con dni"+dni)
                  }
                })
            }
            else{
              console.log("Ya existe una cuenta con dicho IBAN")
              res.status(400).send("Ya existe una cuenta con dicho IBAN")
            }
          }
        })
})
//--
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------


app.delete('/apitechu/v5/eliminarCuenta/',function (req,res){

var dni = req.headers.dni
var iban = req.headers.iban

var query = 'q={"idcliente":"' + dni + '","iban":"' + iban + '"}&'
var filter='f={"_id":1}&'

//creo la varibale cliente que es donde voy a montar mi url a la que haré un get
clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ query +filter + apiKey)
console.log("entro",clienteMlab) //comprueba que se esté formando bien la url
//resM = la respuesta de mlab
//como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
clienteMlab.get('',function(err,resM,body){

  if (!err)
  {

    //si no hay ninguno no me debe devolver nada, sino que devuelva el primero
    if (body.length == 1)
    {
      //vamos a meter en dicho objeto encontrado que el usuario se ha logado. Utilizamos el put
      //el segundo valor del put,del delete y del update es el body, por eso le paso un JSON JSON.parse(cambio)

      clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas/"+ body[0]._id.$oid + "?"+ apiKey)

      clienteMlab.del('',function(err,resM,body)
        {
          console.log('vamos a eliminar una cuenta al cliente con id=', iban)
          if (!err)
          {
              console.log(body)
              console.log(resM.statusCode)
              res.status(200).send(body)
              console.log("Se ha eliminado correctamente la cuenta del usuario con dni",iban)

            }
          else
          {
            console.log("No se ha eliminado la cuenta del usuario con dni",idcliente)
            res.status(400).send("No se ha eliminado la cuenta del usuario con dni"+idcliente)
          }
      })
    }
  }
})
})
//--
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------


app.delete('/apitechu/v5/eliminarCuentas/',function (req,res){

var dni = req.headers.dni

var query = 'q={"idcliente":"' + dni + '"}&'
var filter='f={"_id":1}&'

//creo la varibale cliente que es donde voy a montar mi url a la que haré un get
clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ query +filter + apiKey)
//console.log("entro",clienteMlab) //comprueba que se esté formando bien la url
//resM = la respuesta de mlab
//como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
clienteMlab.get('',function(err,resM,body){

  if (!err)
  {

    //si no hay ninguno no me debe devolver nada, sino que devuelva el primero
    if (body.length > 0)
    {
      //vamos a meter en dicho objeto encontrado que el usuario se ha logado. Utilizamos el put
      //el segundo valor del put,del delete y del update es el body, por eso le paso un JSON JSON.parse(cambio)

  for (var i = 0; i < body.length; i++) {


      clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas/"+ body[i]._id.$oid + "?"+ apiKey)


      clienteMlab.del('',function(err,resM,body)
        {
          console.log('vamos a eliminar las cuenta al cliente con id=', dni)
          if (!err)
          {
              console.log(body)
              console.log(resM.statusCode)

              console.log("Se ha eliminado correctamente la cuenta del usuario con dni",dni)

            }
          else
          {
            console.log("No se ha eliminado la cuenta del usuario con dni",dni)
            res.status(200).send("No se ha eliminado la cuenta del usuario con dni"+dni)
          }
      })
    }
  }
  res.status(200).send("ok")
  }
})
})

//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//devuelve el listado de iban pasando por parametro el id del cliente
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------

app.get ('/apitechu/v5/clienteIban',function (req,res){

var idcliente = req.headers.idcliente
  console.log('vamos a consultar el iban del cliente con id=', idcliente)
  //defino la query donde le voy a pasar el mail y password para ver si ya existe
  var filter='f={"iban":1,"nombrecuenta":1,"saldo":1,"_id":0}'
  var query = 'q={"idcliente":"' + idcliente + '"}&'+filter+'&'
  //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
  clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ query + apiKey)
  //console.log(clienteMlab) //comprueba que se esté formando bien la url
  //resM = la respuesta de mlab
  //como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
    clienteMlab.get('',function(err,resM,body)
    {
      if (!err)
      {
        if (body.length > 0)
        {
          res.status(200).send(body)
          console.log("listado de iban del cliente",idcliente)

        }
        else
        {
          console.log("el cliente",idcliente, "no tiene ningun iban asociado")
          res.status(400).send('el cliente "+idcliente+ " no tiene ningun iban asociado o no existe dicho cliente')

        }
      }
    })
})
//--
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//devuelve el listado de iban pasando por parametro el id del cliente
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------

app.get ('/apitechu/v5/clienteIbanGrafica',function (req,res){

var idcliente = req.headers.idcliente
var array=[]
  console.log('vamos a consultar el iban del cliente con id=', idcliente)
  //defino la query donde le voy a pasar el mail y password para ver si ya existe
  var filter='f={"iban":1,"nombrecuenta":1,"saldo":1,"_id":0}'
  var query = 'q={"idcliente":"' + idcliente + '"}&'+filter+'&'
  var iban=""
  var saldo=""
  //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
  clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ query + apiKey)
  //console.log(clienteMlab) //comprueba que se esté formando bien la url
  //resM = la respuesta de mlab
  //como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
    clienteMlab.get('',function(err,resM,body)
    {
      console.log(body)
      if (!err)
      {

        if (body.length > 0)
        {
          for (var i = 0; i < body.length; i++)
          {


            nombrecuenta=body[i].nombrecuenta
            saldo=Math.abs(body[i].saldo)
            if (i==body.length-1){
                  grafico='["'+nombrecuenta+'",'+saldo+']'
                }
                else{
                  grafico='["'+nombrecuenta+'",'+saldo+'],'
                }
            array+= grafico
          }
        }


      }

          res.send(JSON.parse('['+array+']'))







    })
})

//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//devuelve los movimientos de un usuario pasandole por cabecera el iban
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------

app.get ('/apitechu/v5/movimientos',function (req,res){

//se podria validar que en el parametro que recibimos solo viene un parametro para que no te hayan js inyection
var iban = req.headers.iban
console.log('vamos a consultar los movimientos del cliente con iban=', iban)
//defino la query donde le voy a pasar el mail y password para ver si ya existe
var filter='&f={"movimientos":1,"_id":0}'
var query = 'q={"iban":"' + iban + '"}&s={"fecha":1}'+filter+'&'
//creo la varibale cliente que es donde voy a montar mi url a la que haré un get
clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ query + apiKey)

console.log(clienteMlab) //comprueba que se esté formando bien la url
//resM = la respuesta de mlab
//como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
  clienteMlab.get('',function(err,resM,body)
  {
    if (!err)
    {
      if (body[0].movimientos.length > 0)
      {
        //console.log("body:",body[0])
        res.status(200).send(body)
        console.log("listado de movimientos del iban",iban)


      }
      else
      {
        console.log("el iban",iban, "no tiene movimientos")

        res.status(400).send("el iban "+iban+ " no tiene movimientos")
      }
    }
  })
})
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//devuelve los movimientos de un usuario pasandole por cabecera el iban
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------

app.get ('/apitechu/v5/movimientos_conceptos',function (req,res){

//se podria validar que en el parametro que recibimos solo viene un parametro para que no te hayan js inyection
var iban = req.headers.iban
var pago=0
var nomina=0
var transferencia=0
var compra=0
var venta=0
var con_pago=0
var con_nomina=0
var con_transferencia=0
var con_compra=0
var con_venta=0
console.log('vamos a consultar los movimientos del cliente con iban=', iban)
//defino la query donde le voy a pasar el mail y password para ver si ya existe
var filter='&f={"movimientos":1,"_id":0}'
var query = 'q={"iban":"' + iban + '"}&s={"fecha":1}'+filter+'&'
//creo la varibale cliente que es donde voy a montar mi url a la que haré un get
clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ query + apiKey)

console.log(clienteMlab) //comprueba que se esté formando bien la url
//resM = la respuesta de mlab
//como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
  clienteMlab.get('',function(err,resM,body)
  {
    if (!err)
    {
      if (body[0].movimientos.length > 0)
      {
        for (var i = 0; i < body[0].movimientos.length; i++) {
          if (body[0].movimientos[i].concepto=="Nomina"){
            nomina=nomina+parseInt(body[0].movimientos[i].importe)
            con_nomina=con_nomina+1
          }
          if (body[0].movimientos[i].concepto=="Transferencia"){
            transferencia=transferencia+parseInt(body[0].movimientos[i].importe)
            con_transferencia=con_transferencia+1
          }
          if (body[0].movimientos[i].concepto=="Pago"){
            pago=pago+parseInt(body[0].movimientos[i].importe)
            con_pago=con_pago+1
          }
          if (body[0].movimientos[i].concepto=="Compra"){
            compra=compra+parseInt(body[0].movimientos[i].importe)
            con_compra=con_compra+1
          }
          if (body[0].movimientos[i].concepto=="Venta"){
            venta=venta+parseInt(body[0].movimientos[i].importe)
            con_venta=con_venta+1
          }
        }

        //console.log("body:",body[0])
        res.send([["Transferencia", Math.abs(transferencia),con_transferencia],["Nómina", Math.abs(nomina),con_nomina],["Pago", Math.abs(pago),con_pago],["Compra", Math.abs(compra),con_compra],["Venta", Math.abs(venta),con_venta]])
        console.log(["Transferencia", transferencia],["Nómina", nomina],["Pago", pago])


      }

    }
  })
})
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//devuelve los movimientos de un usuario pasandole por cabecera el iban
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------

app.post ('/apitechu/v5/movimientos/nuevo',function (req,res){

  var idcliente = req.headers.idcliente
  var iban = req.headers.iban
  var importe=req.headers.importe

  var descripcion=req.headers.descripcion
  var concepto=req.headers.concepto
  var detalle=req.headers.detalle
  var longitud=req.headers.longitud
  var latitud=req.headers.latitud
  var ubicacion=req.headers.ubicacion

  var movimientos=""
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();

  if(dd<10) {
      dd='0'+dd
  }
  if(mm<10) {
      mm='0'+mm
  }
  hoy = dd+'/'+mm+'/'+yyyy;



  var query = 'q={"iban":"' + iban + '"}'
  var filter='&f={"movimientos":1,"saldo":1,"_id":0}'
  var saldo=0
  //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
  clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas?"+ query +filter+"&l=1&" + apiKey)
  //console.log(clienteMlab) //comprueba que se esté formando bien la url
  clienteMlab.get('',function(err,resM,body)
  {

        if (!err)
        {

          if (body.length >0)
          {


               if (concepto=="Pago"){

                  saldo=parseInt(body[0].saldo) - parseInt(importe)
                  importe="-"+importe
               }
               if (concepto=="Compra"){

                  saldo=parseInt(body[0].saldo) - parseInt(importe)
                  importe="-"+importe
               }
               else{
                  saldo=parseInt(body[0].saldo) + parseInt(importe)
               }


               var movimientosMax=0

               for (var i = 0; i < body[0].movimientos.length; i++) {
                 if (body[0].movimientos[i].id > movimientosMax){
                  movimientosMax=body[0].movimientos[i].id

                }
              }
              movimientosMax=movimientosMax+1

              var nuevo={"id":movimientosMax,"fecha":hoy,"importe":importe,"descripcion":descripcion,"concepto":concepto,"detalle":detalle,"latitud":latitud,"longitud":longitud,"ubicacion":ubicacion}

              movimientos=body[0].movimientos
               movimientos.push(nuevo)
               var cambio='{"$set":{"movimientos":'+JSON.stringify(movimientos)+'}}'
                clienteMlab=requestJson.createClient(urlMlabRaiz + "cuentas")
                var consulta='?q={"iban":"'+iban +'"}&'+apiKey

              //  clienteMlab.get(consulta+apiKey,JSON.parse(cambio,function(errP,resP,bodyP)

                clienteMlab.put(consulta,JSON.parse(cambio),function(errP,resP,bodyP)
                {
                  //como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
                  //clienteMlab.put('',nuevo,function(err,resM,body){
                    if (!errP)
                    {
                      console.log("movimiento dado de alta")

                    }
                    else{
                      console.log("No se ha dado de alta el nuevo Movimiento")
                      res.status(404).send('No se ha dado de alta el nuevo Movimiento')
                    }
                  })
                  var cambioSaldo='{"$set":{"saldo":'+saldo+'}}'
                  console.log("cambiosaldo",cambioSaldo)
                  clienteMlab.put(consulta,JSON.parse(cambioSaldo),function(errQ,resQ,bodyQ)
                  {
                    //como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
                    //clienteMlab.put('',nuevo,function(err,resM,body){
                      if (!errQ)
                      {
                        console.log("Nuevo movimiento dado de alta y Saldo actualizado")
                        res.status(201).send({"iban":idcliente})
                      }
                      else{
                        console.log("No se ha actualizado el saldo")
                        res.status(404).send('No se ha actualizado el saldo')
                      }
                    })
                }
                else
                {
                  console.log("Se han encontrado varias iban para dicho usuario o no se ha encontrado el iban correspondiente")
                  res.status(404).send('Se han encontrado varias iban para dicho usuario o no se ha encontrado el iban correspondiente')
                }
              }
        else
        {
          console.log("Error en la consulta")
          res.status(404).send('Error en la consulta')
        }
    })
 })



//crea una varibale que le asigno al puerto
var port=process.env.port || 3000
//creo una variable y le asigno el nombre del fichero

//libreria para trabajar con ficheros
var fs=require('fs');

app.listen (port)
console.log("Api escuchando por el puerto " + port);


//var usuarios= require('./clientes.json');
//he cambiado esta llamada para que el test que estoy pasando me funcione, porque el test 'Test de Api usuarios' le estoy diciendo
//que en el fichero deberia haber mail y password pero como estoy utilizando el fichero que no lo tiene pues me va a dar error
// y por eso le cambio a clientes para que me valide estos datos
//var usuarios= require('./usuarios.json');

//var clientes= require('./clientes.json');

//console.log ("Hola mundo")

//var cuentas=require('./cuentas.json');

//------------------------------------------------------------------------------------------------------
//--------NOMBRE:USUARIOS-------------------------------------------------------------------------------
//--------descripcion:FUNCION QUE DEVUELVE TODOS LOS USUARIOS DEL FICHERO clientesBBVA--------------------------------------------------------------
//--------PARAM ENTRADA:NINGUNO-------------------------------------------------------------------------
//--------PARAM SALIDA:TODOS LOS USUARIOS---------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//equivalente a https://api.mlab.com/api/1/databases/bdbancojdea/collections/clientesBBVA?apiKey=Wb9-f2YfLR4yqfzqKpgZPvzFFGoUdk8A
//llamadas desde postman con http://localhost:3000/apitechu/v5/usuarios y asi evitamos tener que pasar por url la apiKey
app.get ('/apitechu/v5/usuarios', function (req,res){
  //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
  clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA?"+apiKey)
  console.log(clienteMlab) //comprueba que se esté formando bien la url
  //resM = la respuesta de mlab
  //como estoy utilizando requestJson por el body me devuelve un json. lo he declarado en la sentencia var clienteMlab=requestJson
  clienteMlab.get('',function(err,resM,body){
    if (!err){
      res.send(body)
    }
  })
})

//------------------------------------------------------------------------------------------------------
//--------NOMBRE:USUARIOS-------------------------------------------------------------------------------
//--------descripcion:FUNCION QUE BUSCA UN USUARIO EN CONCRETO--------------------------------------------------------------
//--------PARAM ENTRADA:DNI  DEL USUARIOA BUSCAR-------------------------------------------------------------------------
//--------PARAM SALIDA:USUARIO ENCONTRADO---------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//vamos a crear otra llamada para la devuelta de un usuario (sólo nombre y apellidos)en concreto pasandole por url pero con mlab y version v5.
//la llamo /apitechu/v5/usuarios/:dni para diferenciarla de la de arriba. me va a venir por parametro por eso los :
//equivalente a https://api.mlab.com/api/1/databases/bdbancojdea/collections/clientesBBVA?q={"dni":"12345678A"}&apiKey=Wb9-f2YfLR4yqfzqKpgZPvzFFGoUdk8A
//llamadas desde postman con http://localhost:3000/apitechu/v5/usuarios/2 y asi evitamos tener que pasar por url la apiKey
//devuelve el array con un usuario, asi que si quiero que solo me devuelva uno, le tengo que pasar el parametro en la query l=1
app.get ('/apitechu/v5/usuarios/:dni', function (req,res)
{
  var dni=req.params.dni
  //defino la query
  var query='q={"dni":"'+dni+ '"}&f={"dni":1,"nombre":1,"apellidos":1,"email":1,"_id":0}'
  //creo la varibale cliente que es donde voy a montar mi url a la que haré un get
  clienteMlab=requestJson.createClient(urlMlabRaiz + "clientesBBVA?"+ query +"&l=1&" + apiKey)
  console.log(clienteMlab) //comprueba que se esté formando bien la url
  //resM = la respuesta de mlab
  clienteMlab.get('',function(err,resM,body)
  {
    if (!err)
    {
      //si no hay ninguno no me debe devolver nada, sino que devuelva el primero
      if (body.length == 0)
      {
        console.log("no hay ningun usuario con el dni=",dni)

        res.status(404).send('usuario no encontrado')
      }
      else
      {
        console.log("el usuario con dni=",dni,"se ha encontrado correctamente")
        //console.log(body[0])
        res.status(200).send(body[0])

          //sino pongo en la busqueda el f, se podría hacer lo siguiente
          //res.send({"nombre":body[0].nombre),"apellidos":body[0].appelido)}
      }
    }
  })
})
